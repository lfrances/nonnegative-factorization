# Lnn-fac: Lightweight nn-fac with only nmf, and few dependencies

For easy web integration. If you are interested in using NMF, check the original package [nn-fac](https://gitlab.inria.fr/amarmore/nonnegative-factorization).

## References
[1] N. Gillis and F. Glineur, "Accelerated Multiplicative Updates and Hierarchical ALS Algorithms for Nonnegative Matrix Factorization," Neural Computation 24 (4): 1085-1105, 2012.

[2] D. D. Lee and H. S. Seung, "Learning the parts of objects by non-negative matrix factorization," Nature, vol. 401, no. 6755, p. 788, 1999.
